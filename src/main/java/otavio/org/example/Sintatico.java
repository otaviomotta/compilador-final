package otavio.org.example;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Sintatico {
    private Lexico lexico;
    private Token token;
    private int linha;
    private int coluna;


    private TabelaSimbolos tabela;
    private int endereco;
    private int contRotulo = 1;
    private List<Registro> ultimasVariaveisDeclaradas = new ArrayList<>();
    private String nomeArquivoSaida;
    private String caminhoArquivoSaida;
    private BufferedWriter bw;
    private FileWriter fw;
    private String rotulo = "";
    private String rotElse;

    public void ReconhecerToken(){
        token = lexico.getToken(linha, coluna);
        coluna = token.getColuna() + token.getTamanhoToken();
        linha = token.getLinha();
        System.out.println(token);
    }

    public Sintatico(String nomeArquivo){
        linha = 1;
        coluna = 1;
        this.lexico = new Lexico(nomeArquivo);
    }

    public void Reconhecer()
    {
        ReconhecerToken();

        this.endereco = 0;

        nomeArquivoSaida = "CODIGO_EM_C.c";
        caminhoArquivoSaida = Paths.get(nomeArquivoSaida).toAbsolutePath().toString();

        bw = null;
        fw = null;

        try {
            fw = new FileWriter(caminhoArquivoSaida, Charset.forName("UTF-8"));
            bw = new BufferedWriter(fw);
            programa();
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(this.tabela);
    }

    private String criarRotulo(String texto) {
        String retorno = "rotulo" + texto + contRotulo;
        contRotulo++;
        return retorno;
    }

    private void gerarCodigo(String instrucoes) {
        try {
            if (rotulo.isEmpty()) {
                bw.write(instrucoes + "\n");
            } else {
                bw.write(rotulo + ": " +  instrucoes + "\n");
                rotulo = "";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void mensagemErro(String msg) {
        System.err.println("Linha: " + token.getLinha() +
                ", Coluna: " + token.getColuna() +
                msg);
    }

    public void programa() {
        if ((token.getClasse() == Classe.cPalRes)
                && (token.getValor().getValorIdentificador().equalsIgnoreCase("program"))) {
            ReconhecerToken();
            if (token.getClasse() == Classe.cId) {
                ReconhecerToken();
                Acao1();
                corpo();
                if (token.getClasse() == Classe.cPonto) {
                    ReconhecerToken();
                } else {
                    mensagemErro(" -> não está encerrando com ponto '.'");
                }
                Acao2();
            } else {
                mensagemErro(" -> o programa não possui nome");
            }
        } else {
            mensagemErro(" -> não está começando com 'Program'");
        }
    }

    public void Acao1()
    {
        tabela=new TabelaSimbolos();

        tabela.setTabelaPai(null);

        Registro registro=new Registro();
        registro.setNome(token.getValor().getValorIdentificador());
        registro.setCategoria(Categoria.PROGRAMAPRINCIPAL);

        registro.setNivel(0);
        registro.setOffset(0);
        registro.setTabelaSimbolos(tabela);
        registro.setRotulo("main");
        tabela.inserirRegistro(registro);

        String codigo = "#include <stdio.h>\n" +
                "\nint main(){\n";

        gerarCodigo(codigo);
    }

    public void Acao2()
    {
        Registro registro=new Registro();
        registro.setNome(null);
        registro.setCategoria(Categoria.PROGRAMAPRINCIPAL);
        registro.setNivel(0);
        registro.setOffset(0);
        registro.setTabelaSimbolos(tabela);
        registro.setRotulo("finalCode");
        tabela.inserirRegistro(registro);
        String codigo = "\n}\n";
        gerarCodigo(codigo);
    }

    public void corpo() {
        declara();
        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))) {
            ReconhecerToken();
            sentencas();
            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))) {
                ReconhecerToken();
            }else {
                mensagemErro(" -> não está encerrando com 'End'");
            }
        }else {
            mensagemErro(" -> não possui 'Begin' no corpo do programa");
        }
    }

    public void declara() {
        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("var"))) {
            ReconhecerToken();
            dvar();
            mais_dc();
        }
    }

    public void mais_dc() {
        if (token.getClasse() == Classe.cPontoVirgula) {
            ReconhecerToken();
            cont_dc();
        } else {
            mensagemErro(" -> a instrução não possui ponto e vírgula ';'");
        }
    }

    public void cont_dc() {
        if (token.getClasse() == Classe.cId) {
            dvar();
            mais_dc();
        }
    }

    public void dvar() {
        variaveis();
        if (token.getClasse() == Classe.cDoisPontos) {
            ReconhecerToken();
            tipo_var();
        } else {
            mensagemErro(" -> a instrução não possui dois pontos ':'");
        }
    }

    private void Acao3(String type) {
        String codigo= '\t'+type;
        for(int i=0;i<this.ultimasVariaveisDeclaradas.size();i++)
        {
            codigo=codigo+' '+ this.ultimasVariaveisDeclaradas.get(i).getNome();
            if(i == this.ultimasVariaveisDeclaradas.size()-1)
            {
                codigo=codigo + ';';
            }
            else{
                codigo=codigo + ',';
            }
        }
        gerarCodigo(codigo);
    }

    public void Acao4()
    {
        Registro registro=new Registro();
        registro.setNome(token.getValor().getValorIdentificador());
        registro.setCategoria(Categoria.VARIAVEL);
        registro.setNivel(0);
        registro.setOffset(0);
        registro.setTabelaSimbolos(tabela);
        this.endereco++;
        registro.setRotulo("variavel"+this.endereco);
        ultimasVariaveisDeclaradas.add(registro);
        this.tabela.inserirRegistro(registro);
    }

    public void tipo_var() {
        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("integer"))) {
            Acao3("int");
            ReconhecerToken();
        }else if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("real"))) {
            Acao3("float");
            ReconhecerToken();
        }else {
            mensagemErro(" -> o tipo Integer não está sendo declarado");
        }
    }

    public void variaveis() {
        if (token.getClasse() == Classe.cId) {
            Acao4();
            ReconhecerToken();
            mais_var();
        }else {
            mensagemErro(" -> não possui identificador");
        }
    }

    public void mais_var(){
        if (token.getClasse() == Classe.cVirgula) {
            ReconhecerToken();
            variaveis();
        }
    }


    public void sentencas() {
        comando();
        mais_sentencas();
    }


    public void mais_sentencas() {
        if (token.getClasse() == Classe.cPontoVirgula) {
            ReconhecerToken();
            cont_sentencas();
        }else {
            mensagemErro(" -> a instrução não possui ponto e vírgula ';'");
        }
    }

    public void cont_sentencas() {
        if (((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("read"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("write"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("for"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("repeat"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("while"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("if"))) ||
                ((token.getClasse() == Classe.cId))
        ) {
            sentencas();
        }
    }

    public List<Token> var_read(List<Token> vetorTokens) {
        if (token.getClasse() == Classe.cId) {
            vetorTokens.add(token);
            ReconhecerToken();
            vetorTokens = mais_var_read(vetorTokens);
        }else {
            mensagemErro(" -> não possui identificador");
        }

        return vetorTokens;
    }

    public List<Token> mais_var_read(List<Token> vetorTokens) {
        if (token.getClasse() == Classe.cVirgula) {
            ReconhecerToken();
            vetorTokens = var_read(vetorTokens);
        }

        return vetorTokens;
    }

    public String var_write(String codigo) {
        if (token.getClasse() == Classe.cId) {
            codigo = codigo + token.getValor().getValorIdentificador();
            ReconhecerToken();
            codigo = mais_var_write(codigo);
        }else {
            mensagemErro(" -> não possui identificador");
        }

        return codigo;
    }

    public String mais_var_write(String codigo) {

        if (token.getClasse() == Classe.cVirgula) {
            codigo = codigo + ',';
            ReconhecerToken();
            codigo = var_write(codigo);
        }

        return codigo;
    }

    public void comando() {

        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("read"))){
            String codigo="\tscanf";
            ReconhecerToken();
            if (token.getClasse() == Classe.cParEsq) {
                codigo=codigo+"(\"";
                ReconhecerToken();
                List<Token> arrayToken = new ArrayList<Token>();
                arrayToken=var_read(arrayToken);
                for(Token i: arrayToken){
                    codigo=codigo+"%d ";
                }
                codigo=codigo+"\", ";
                for(Token i: arrayToken){
                    if(i == arrayToken.get(arrayToken.size()-1)){
                        codigo=codigo+"&"+i.getValor().getValorIdentificador();
                    }else{
                        codigo=codigo+"&"+i.getValor().getValorIdentificador()+", ";
                    }
                }
                if (token.getClasse() == Classe.cParDir) {

                    codigo=codigo+");";
                    gerarCodigo(codigo);
                    ReconhecerToken();
                }else {
                    mensagemErro(" -> não possui parêntese direito ')'");
                }
            }else {
                mensagemErro(" -> não possui parêntese esquerdo '('");
            }
        }else

            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("write"))){
                String referencias="\tprintf";
                String codigo = "";
                ReconhecerToken();
                if (token.getClasse() == Classe.cParEsq) {
                    referencias = referencias + "(\"";
                    ReconhecerToken();

                    codigo=codigo+var_write("");

                    if (codigo.length() >  0) {
                        referencias = referencias + "%d ".repeat(codigo.split(",").length);
                        referencias = referencias + "\", ";
                    } else {
                        referencias = referencias + "\"";
                    }

                    if (token.getClasse() == Classe.cParDir) {
                        codigo=codigo+");";
                        gerarCodigo(referencias + codigo);
                        ReconhecerToken();
                    }else {
                        mensagemErro(" -> não possui parêntese direito ')'");
                    }
                }else {
                    mensagemErro(" -> não possui parêntese esquerdo '('");
                }
            }else

            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("for"))){
                String codigo="\n\tfor(";
                ReconhecerToken();
                if (token.getClasse() == Classe.cId) {
                    String identificador = token.getValor().getValorIdentificador();
                    codigo=codigo+identificador;
                    ReconhecerToken();

                    if (token.getClasse() == Classe.cAtribuicao){
                        codigo=codigo+"=";
                        ReconhecerToken();

                        codigo=codigo+expressao();

                        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("to"))){
                            codigo=codigo+";";
                            ReconhecerToken();
                            codigo=codigo+identificador;
                            codigo=codigo+"<="+expressao()+";";
                            codigo=codigo+identificador + "++)";
                            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("do"))){
                                ReconhecerToken();
                                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                                    codigo=codigo+"{";
                                    gerarCodigo(codigo);
                                    ReconhecerToken();
                                    sentencas();
                                    if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                                        String codigoFinal = "\t}";
                                        gerarCodigo(codigoFinal);
                                        ReconhecerToken();
                                    }else {
                                        mensagemErro(" -> não possui 'End' no For");
                                    }
                                }else {
                                    mensagemErro(" -> não possui 'Begin' no For");
                                }
                            }else {
                                mensagemErro(" -> não possui 'Do' no For");
                            }
                        }else {
                            mensagemErro(" -> não possui 'To' no For");
                        }
                    }else {
                        mensagemErro(" -> não possui ':=' no For");
                    }
                }else {
                    mensagemErro(" -> não possui identificador no início do For");
                }
            }else

            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("repeat"))){
                String codigo="\n\tdo {\n\t";
                ReconhecerToken();
                gerarCodigo(codigo);
                sentencas();
                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("until"))){

                    ReconhecerToken();
                    if (token.getClasse() == Classe.cParEsq){
                        String codigoFinal="\n\t}while";
                        codigoFinal=codigoFinal+"(";
                        ReconhecerToken();
                        codigoFinal=codigoFinal+condicao();

                        if (token.getClasse() == Classe.cParDir){
                            codigoFinal=codigoFinal+");";
                            gerarCodigo(codigoFinal);
                            ReconhecerToken();
                        }else {
                            mensagemErro(" -> não fechou parênteses no Repeat");
                        }
                    }else {
                        mensagemErro(" -> não abriu parênteses no Repeat");
                    }
                }else {
                    mensagemErro(" -> não possui 'Until' no Repeat");
                }
            }

            else if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("while"))){
                String codigo="\n\twhile";
                ReconhecerToken();

                if (token.getClasse() == Classe.cParEsq){
                    codigo=codigo+"(";
                    ReconhecerToken();
                    codigo=codigo+condicao();
                    if (token.getClasse() == Classe.cParDir){
                        codigo=codigo+")";
                        ReconhecerToken();

                        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("do"))){
                            ReconhecerToken();
                            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                                codigo=codigo+"{\n";
                                gerarCodigo(codigo);
                                ReconhecerToken();
                                sentencas();
                                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                                    codigo="\t}\n";
                                    gerarCodigo(codigo);
                                    ReconhecerToken();

                                }else {
                                    mensagemErro(" -> não possui 'End' no While");
                                }
                            }else {
                                mensagemErro(" -> não possui 'Begin' no While");
                            }
                        }else {
                            mensagemErro(" -> não possui 'Do' no While");
                        }
                    }else {
                        mensagemErro(" -> não possui ')' no While");
                    }
                }else {
                    mensagemErro(" -> não possui '(' no While");
                }
            }
            else if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("if"))){
                String codigo="";
                ReconhecerToken();
                if (token.getClasse() == Classe.cParEsq){
                    codigo=codigo+"\n\tif(";
                    ReconhecerToken();
                    codigo=codigo+condicao();
                    if (token.getClasse() == Classe.cParDir){
                        codigo=codigo+")";
                        ReconhecerToken();

                        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("then"))){
                            ReconhecerToken();
                            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                                codigo=codigo +" {";
                                gerarCodigo(codigo);
                                ReconhecerToken();
                                sentencas();
                                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                                    ReconhecerToken();

                                    String codigoFinal = "";
                                    codigoFinal = codigoFinal + "\t}";
                                    gerarCodigo(codigoFinal);
                                    pfalsa();
                                }else {
                                    mensagemErro(" -> não possui 'End' no While");
                                }
                            }else {
                                mensagemErro(" -> não possui 'Begin' no While");
                            }
                        }else {
                            mensagemErro(" -> não possui 'Do' no While");
                        }
                    }else {
                        mensagemErro(" -> não possui ')' no While");
                    }
                }else {
                    mensagemErro(" -> não possui '(' no While");
                }
            }
            else if (token.getClasse() == Classe.cId){
                String codigo="\n\t";
                codigo=codigo+token.getValor().getValorIdentificador();
                ReconhecerToken();

                if (token.getClasse() == Classe.cAtribuicao){
                    codigo=codigo+"=";
                    ReconhecerToken();
                    codigo=codigo+expressao()+";";
                    gerarCodigo(codigo);
                }
                else {
                    mensagemErro(" -> não possui atribuição");
                }
            }
    }

    public String condicao() {
        String expressao1 = expressao();
        String relacao = relacao();
        String expressao2 = expressao();

        return expressao1 + relacao + expressao2;
    }

    public void pfalsa() {
        String codigo = "";

        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("else"))){
            codigo = codigo + "\telse";
            ReconhecerToken();
            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                codigo = codigo + "{";
                gerarCodigo(codigo);
                ReconhecerToken();
                sentencas();
                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                    String codigoFinal = "\n\t}";
                    gerarCodigo(codigoFinal);
                    ReconhecerToken();
                }else {
                    mensagemErro(" -> não está encerrando com 'End'");
                }
            }else {
                mensagemErro(" -> não está começando com 'Begin'");
            }
        }
    }

    public String relacao() {
        String operador="";
        if (token.getClasse() == Classe.cIgual) {
            operador="=";
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMaior) {
            operador=">";
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMenor) {
            operador="<";
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMaiorIgual) {
            operador = ">=";
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMenorIgual) {
            operador = "<=";
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cDiferente) {
            operador = "!=";
            ReconhecerToken();
        }else {
            mensagemErro(" -> não possui o operador de relação");
        }

        return operador;
    }

    public String expressao() {
        String termo = termo();
        String outrosTermos = outros_termos();

        return termo + outrosTermos;
    }

    public String outros_termos() {
        String op = "";
        String termo= "";
        String outrosTermos = "";

        if (token.getClasse() == Classe.cMais || token.getClasse() == Classe.cMenos) {
            op = op_ad();
            termo = termo();
            outrosTermos = outros_termos();
        }

        return op + termo + outrosTermos;
    }

    public String op_ad() {
        String op = "";
        if (token.getClasse() == Classe.cMais) {
            op = "+";
            ReconhecerToken();
        } else if (token.getClasse() == Classe.cMenos) {
            op = "-";
            ReconhecerToken();
        }else {
            mensagemErro(" -> não possui o operador de adição ou subtração");
        }
        return op;
    }

    public String termo() {
        String fator = fator();
        String maisFatores = mais_fatores();

        return fator + maisFatores;
    }


    public String mais_fatores() {
        if (token.getClasse() == Classe.cMultiplicacao || token.getClasse() == Classe.cDivisao) {
            String op = op_mul();
            String fator = fator();
            String outrosFatores = mais_fatores();

            return op + fator + outrosFatores;
        }

        return "";
    }

    public String op_mul() {
        String op = "";
        if (token.getClasse() == Classe.cMultiplicacao) {
            op = "*";
            ReconhecerToken();
        } else if (token.getClasse() == Classe.cDivisao) {
            op = "/";
            ReconhecerToken();
        } else {
            mensagemErro(" -> não possui o operador de multiplicação ou divisão");
        }

        return op;
    }
    
    public String fator() {
        String retornaFator = "";

        if (token.getClasse() == Classe.cId) {
            retornaFator = token.getValor().getValorIdentificador();
            ReconhecerToken();
        } else if (token.getClasse() == Classe.cInt) {
            retornaFator = String.valueOf(token.getValor().getValorInteiro());
            ReconhecerToken();
        } else if (token.getClasse() == Classe.cReal) {
            retornaFator = String.valueOf(token.getValor().getValorDecimal());
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cParEsq){
            retornaFator="(";
            ReconhecerToken();
            retornaFator = retornaFator + expressao();
            if (token.getClasse() == Classe.cParDir){
                retornaFator= retornaFator + ")";
                ReconhecerToken();
            }else {
                mensagemErro(" -> não possui parêntese direito ')'");
            }
        }else {
            mensagemErro(" -> não possui fator 'In Num Exp'");
        }

        return retornaFator;
    }
}
